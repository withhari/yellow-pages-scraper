var Nightmare = require('nightmare');
var fs = require('fs');
var express = require('express');
var path = require('path');
var app = express();

/**
 * Few TODOs:
 *  Add a global varible to control state of app
 *  Add reset to make sure the search can be interrupted, using routes like /app/reset
 *  Be more specific in job to collect more results
 */

var creator = Nightmare({
    //set to true in your local PC to see what's going on
    show: true,
    waitTimeout: 120000
});

var currentIndex = 1;

const _url = 'https://www.yellowpages.com.au/search/listings?clue=%term%&locationClue=&lat=&lon=&referredBy=www.yellowpages.com.au&selectedViewMode=&eventType=refinement&state=%state%';
var categories = [];
const _nxt = 'https://www.yellowpages.com.au/search/listings?clue=%term%&eventType=pagination&pageNumber=%page%&referredBy=www.yellowpages.com.au&state=%state%'
//Architects left: VIC QLD NSW
//Interior designers left: NSW VIC QLD
//Window fabrication left: NSW QLD
const states = [
    'VIC', /*'NSW','QLD',*/ 'WA', 'SA', 'TAS', 'ACT', 'NT'
];

var tempStates = states, currentState;

app.get('/', function (req, res) {
    res.sendFile(path.join(__dirname, 'index.html'));
});

app.get('/crawl/:word', function (req, res) {
    tempStates = states;
    startCrawling(req.params.word);
});

app.get('/:filename', function (req, res) {
    var file = path.join(__dirname, req.params.filename + '.csv');
    if (fs.existsSync(file)) {
        res.send(
            fs.readFileSync(file).toString()
        )
    } else {
        res.send('Please wait...');
    }
});

app.listen(3300, function () {
    console.log("Please goto: http://localhost:3300 on your browser");
});

function startCrawling(topic) {
    if (topic == undefined) {
        readAndSave(getNextUrl());
        return;
    }

    if (tempStates.length == 0) {
        console.log('All done!');
        return;
    }
    currentIndex = 1;
    currentState = tempStates.pop();
    //start with first page
    //then it will go through all 29 pages.
    //then again startCrawling is called for next step
    readAndSave(_url.replace("%term%", topic).replace('%state%', currentState), topic);
}

function getNextUrl() {
    var urls = fs.readFileSync('urls.txt').toString().split('\n');
    if (urls.length > 0) {
        var url = urls.pop().trim();
        fs.writeFileSync('urls.txt', urls.join('\n'));
        return url;
    }
    return null;
}

//if the word parameter is passed then its from browser.
//else its from command line with argument url i.e use-url
//node index.js use-url
function readAndSave(url, word) {
    if (url == null || url.length == 0) return;
    console.log(url);
    creator
        .goto(url)
        .wait('#search-results-page')
        .evaluate(function (index) {
            //we need to see if there are any more page
            index++;
            var carry = false;
            var pagination = document.querySelectorAll('.pagination');
            for (var i = 0; i < pagination.length; i++) {
                if (parseInt(pagination[i].getAttribute('data-page')) === index) {
                    carry = 'https://www.yellowpages.com.au' + pagination[i].getAttribute('href');
                }
            }
            var cmps = [];
            var allList = document.getElementsByClassName('search-results')[0].getElementsByClassName('listing-data');
            for (var i = 0; i < allList.length; i++) {
                try {
                    var res = [];
                    if (allList[i].getElementsByClassName('listing-name').length > 0) {
                        res.push(allList[i].getElementsByClassName('listing-name')[0].innerText);
                    } else {
                        res.push(' ');
                    }
                    if (allList[i].getElementsByClassName('contact-email').length > 0) {
                        res.push(allList[i].getElementsByClassName('contact-email')[0].getAttribute('data-email'));
                    } else {
                        res.push(' ');
                    }
                    if (allList[i].getElementsByClassName('contact-phone').length > 0) {
                        res.push(allList[i].getElementsByClassName('contact-phone')[0].getAttribute('href').replace('tel:', ''));
                    } else {
                        res.push(' ');
                    }
                    if (cmps.indexOf(res) === -1) {
                        cmps.push(res.join());
                    }
                } catch (e) {

                }
            }
            return { data: cmps, carryOn: carry };
        }, currentIndex)
        .then(function (result) {
            fs.appendFile(word || getNameFromUrl(url) + '.csv', '\r\n' + result.data.join('\r\n'), function (err) {
                if (err) {
                    return console.log(err);
                }
            });
            if (!result.carryOn) {
                currentIndex = 1;
                startCrawling(word);
                return;
            }
            currentIndex++;
            console.log('Done: ' + currentIndex);
            if (word != undefined) {
                readAndSave(_nxt.replace('%term%', word).replace('%page%', currentIndex).replace('%state%', currentState), word);
            } else {
                //go with next page URL
                readAndSave(result.carryOn);
            }
        })
        .catch(function (error) {
            if (error) {
                saveErrorUrl(url);
                startCrawling(word);
            }
        });
}

function saveErrorUrl(url) {
    fs.appendFile('error-url.txt', url + '\r\n', function(error) {

    })
}

if (process.argv.length >= 3 && process.argv[2] == 'use-url') {
    startCrawling();
}

function getNameFromUrl(url) {
    return url.split('?clue=')[1].split('&')[0];
}